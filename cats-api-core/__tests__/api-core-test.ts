import Client from '../../dev/http-client';
import type {Cat, CatMinInfo, CatsList, GroupedCats} from '../../dev/types';

const HttpClient = Client.getInstance();

const cats: CatMinInfo[] = [{ name: 'Суперавтотестер', description: '', gender: 'male' }];

let catId;

const fakeId = 'invalid_id';

describe('Тестирование API для котов', () => {
    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            const body = add_cat_response.body as CatsList;
            if (body.cats[0].id) {
                catId = body.cats[0].id;
            } else {
                throw new Error('Не получилось получить id тестового котика!');
            }
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов: ' + error.message);
        }
    });

    afterAll(async () => {

        await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
    });


    // № 1 Автоматизировать тест на поиск существующего котика по id

    it('Должен найти котика по его ID', async () => {
        const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);
        const expectedCat: Cat = {
            id: catId,
            name: cats[0].name,
            description: expect.any(String),
            gender: cats[0].gender,
            tags: null,
            likes: expect.any(Number),
            dislikes: expect.any(Number)
        };

        expect(response.body).toEqual({ cat: expectedCat });
    });

    // № 2 Автоматизировать тест на поиск некорректного id котика

    it('Должен вернуть ошибку при поиске котика по некорректному ID', async () => {
        await expect(
            HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
                responseType: 'json',
            })
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });


    // № 3 Автоматизировать тест на проверку добавления описания котику

    it('Должен добавить описание к котику', async () => {
        const catDescription = 'Новое описание котика';
        const response = await HttpClient.post('core/cats/save-description', {
            responseType: 'json',
            json: { catId, catDescription },
        });
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            id: catId,
            name: cats[0].name,
            description: catDescription,
            tags: null,
            gender: cats[0].gender,
            likes: expect.any(Number),
            dislikes: expect.any(Number),
        });
    });

    // № 4 Автоматизировать тест на получение списка котов сгруппированных по группам

    it('Должен получить список котов, сгруппированных по группам', async () => {
        const response = await HttpClient.get(`core/cats/allByLetter`, {
            responseType: 'json',
        });
        const expectedResponse: GroupedCats = {
            groups: expect.arrayContaining([
                expect.objectContaining({
                    title: expect.any(String),
                    cats: expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            description: expect.any(String),
                            tags: null,
                            gender: expect.any(String),
                            likes: expect.any(Number),
                            dislikes: expect.any(Number),
                            count_by_letter: expect.any(String),
                        })
                    ]),
                    count_in_group: expect.any(Number),
                    count_by_letter: expect.any(Number),
                })
            ]),
            count_all: expect.any(Number),
            count_output: expect.any(Number)
        };
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual(expectedResponse);
    });
});

