/**
 * Набор минимальной информации для добавления кота
 * @property name - имя
 * @property description - описание
 * @property gender - пол
 */
export type CatMinInfo = {
  name: string;
  description: string;
  gender: 'male' | 'female' | 'unisex';
};

/**
 * Полная информация о коте
 * @property name - имя
 * @property description - описание
 * @property gender - пол
 * @property id - id
 * @property tags - теги
 * @property likes - количество лайков
 * @property dislikes - количество дизлайков
 * @property [message=] - сообщение
 */
export type Cat = CatMinInfo & {
  id: number;
  tags: string;
  likes: number;
  dislikes: number;
  message?: string;
};
export type GroupedCats = { // Ответ при успешном поиске
  groups: {
    title: string;
    cats: Cat[];
    count_in_group: number;
    count_by_letter: number;
  }[];
  count_all: number;
  count_output: number;
};
/**
 * Ответ при успешном добавлении кота
 */
export type CatsList = { cats: Cat[] };
